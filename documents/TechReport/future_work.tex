Perhaps the most promising direction that our work on Galant can take is that
of developing research applications.  The benefits are twofold.  First,
Galant has the potential for providing a rich environment for the exploration
of new graph algorithms or developing hypotheses about the behavior of
existing ones.  Second, as Galant is augmented with the infrastructure for
specific research domains (in the form of additional Java classes), some of the
resulting functionality can be migrated into its core. Or the core
will be enhanced to accommodate new capabilities. The former
has happened with research on crossing
minimization heuristics (e.g.,~\cite{2012-JEA-Stallmann}).\footnote{
  Working versions of these heuristics are available in Version 3.3 only\\ --
  see \mbox{\url{https://bitbucket.org/stallmann/galant-3.3}.}
}

One drawback of the current implementation is
that, unlike GDR, it provides no facility for user interaction other than stepping forward
and back.
For example, one might want the user to be able to select the next node to
visit during a depth-first search or a node to insert or remove during an animation of
a binary search tree.
A query window like that of GDR would be a useful enhancement, although the semantics of
repeating a display state during which a query occurs would need to be worked out
(GDR has no such problem because it does not allow backward steps).\footnote{
  The latest release has a simple mechanism for selecting the next node or edge
  via a query window that asks for a node id or the id's of endpoints of two edges.
  The query does not reappear regardless of backward and forward stepping;
  the initial response to it is used.
}

  Because these node and edge states both guide the logic of the algorithm
  and how the nodes/edges are displayed, the nomenclature has become awkward.
  A better way to handle the situation is to add initial declarations
  that specify color, thickness, and, in case of nodes, fill color, for each logical state.
  A logical state would have a name, e.g., \textsf{inTree},
  and be automatically provided with a setter (Boolean argument),
  e.g., \textsf{setInTree}, and a logical test, e.g., \textsf{isInTree}.

A key challenge confronting any developer of algorithm animation
software is that of accessibility to blind users.
Previous work addressed this via a combination \emph{earcons}\footnote{
\emph{Earcons} are sounds that signal specific events, such as the arrival of email. The term was coined by Blattner et al.~\cite{1989-HCI-Blattner-earcons}.
}, spoken navigation
and haptic interfaces
(see~\cite{2002-SoftViz-Baloian,2005-SCCC-Baloian,2002-Diagrams-Bennett}).
The resulting algorithm animations were developed for demonstration and exploration rather than simplified
creation of
animations.
In theory any graph navigation tool can be extended, with appropriate auditory
signals for steps in an animation, to an algorithm animation tool.
The most promising recent example, due to its simplicity, is GSK~\cite{2013-SIGCSE-Balik}.
Earcons can be added to substitute for state changes of nodes or edges.

A user study testing the hypothesis that student creation of animations
promotes enhanced learning raises several nontrivial questions.
Are we going to measure ability to navigate specific algorithms? Or a broader
understanding of graphs and graph algorithms?
Can we make a fair comparison that takes into account the
extra effort expended by students to create and debug animations?
Why incur the overhead of designing an experiment that is very likely to validate the obvious?
Namely:
in order to create a compelling animation,
an animator must first decide what aspects of a graph are important
at each step of an algorithm and then how best to highlight these.
This two-stage process requires a longer and more intense involvement
with an algorithm than mere exploration of an existing animation.

There are various implementation issues with and useful enhancements
to the current version of Galant.
These will be addressed in future releases.
As new animations for teaching and research
are created, other issues and desired enhancements
will undoubtedly arise.
The current implementation should be transparent and flexible enough to effect the necessary modifications --- the most challenging aspect
of creating enhancements has been and continues to be the design decisions involved.

% [Last modified: 2013 06 26 at 19:02:26 GMT]
