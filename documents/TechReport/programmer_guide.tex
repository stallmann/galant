Animation programmers can write algorithms in notation that resembles
textbook pseudocode
in files that have a \texttt{.alg} extension.
The animation examples have used procedural syntax for function calls, as in, for example,
\texttt{setWeight(v,0)}.
Java (object oriented) syntax can also be used: \texttt{v.setWeight(0)}.
A key advantage of Galant is that a seasoned Java programmer can
not only use the Java syntax but can also augment Galant algorithms with
arbitrary Java classes defined externally, using \texttt{import} statements.
All Galant code is effectively Java, either natively, or via macro preprocessing.

The text panel provides a crude editor for algorithms (as well as GraphML
descriptions of graphs);
its limited capabilities make it useful primarily for fine tuning and error correction.
The first author uses emacs in Java mode to edit algorithms offline, for example,
not a major inconvenience -- it is easy to reload algorithms when they are modified
without exiting Galant.
The Galant editor is also useful in that it provides syntax highlighting of Galant
functions and macros.

The source code for an algorithm begins with any number (including none)
of global variable declarations and function definitions.
This is followed by the code for the algorithm itself, starting with the keyword
\textsf{algorithm}.
A \emph{code block}
is a sequence of statements, each terminated by a semicolon, just as in
Java.
The main algorithm has the form
\begin{quote}
\texttt{algorithm \{}\\
\hspace*{2em}\emph{code block}\\
\texttt{\}}
\end{quote}

Declarations of global variables are also like those of Java:\\
\hspace*{1em}\emph{type} \emph{variable\_name}\texttt{;}\\
or\\
\hspace*{1em}\emph{type} \texttt{[]} \emph{variable\_name}\texttt{;}\\
to declare an array of the given type.
All variables must be initialized either within a function definition or
in the algorithm.
The Java incantation\\
\hspace*{1em}\emph{type} \emph{variable\_name}
\texttt{= new} \emph{type}\texttt{[} \emph{size} \texttt{]}\\
is used to initialize an array with \emph{size} elements initialized to \textsf{null}
or 0.
Arrays use 0-based indexing: the largest index is $\mathit{size} - 1$.
Detailed
information about function declarations is in Section~\ref{sec:functions}
below.

Central to the Galant API is the \texttt{graph} object: currently all other
parts of the API refer to it.
The components of a graph are declared to be of type \texttt{Node} or
\texttt{Edge} and can be accessed/modified via a variety of
functions/methods.
When an observer or explorer interacts with the animation they move either
forward or backward one step at a time.
All aspects of the graph API therefore refer to the current \emph{state of
  the graph}, the set of states behaving as a stack.
API calls that change the state of a node or an edge automatically
generate a next step,
but the programmer can override this using a \texttt{beginStep()} and
\texttt{endStep()} pair. For example, the beginning of our implementation of
Dijkstra's algorithm looks like
\begin{verbatim}
beginStep();
for_nodes(node) {
    setWeight(node, INFINITY);
    nodePQ.add(node);
}
endStep();
\end{verbatim}
Without the \texttt{beginStep}/\texttt{endStep}
override, this initialization would require the observer to click
through multiple steps (one for each node) before getting to the interesting
part of the animation.

\input{Y_graph_functions}

\input{Y_utility_functions}

Functions and macros for the graph as a whole are shown in Table~\ref{tab:graph_functions}, while Table~\ref{tab:utility_functions} lists some algorithm functions not related to any aspect of a graph.

The nodes and edges, of type \textsf{Node} and \textsf{Edge}, respectively,
are subtypes/extensions of \textsf{GraphElement}.
Arbitrary attributes can be assigned to each graph element. In the GraphML file
these show up as, for example,\\
\hspace*{3em}
\textsf{
$<$node \emph{attribute\_1}="\emph{value\_1}" ... \emph{attribute\_k}="\emph{value\_k}" /$>$
}

Each node and edge has a unique integer id.
The id's are assigned consecutively as nodes/edges are created;
they may not be altered.
The id of a node or edge can be accessed via the \texttt{id()} function.
Often, as is the case with the depth-first search algorithm, it makes sense to use
arrays indexed by node or edge id's.
Since graphs may be generated externally and/or have undergone deletions of nodes or
edges, the id's are not always contiguous.
The functions \texttt{nodeIds()} and \texttt{edgeIds()} return the size of an array
large enough to accommodate the appropriate id's as indexes. So code such as

\begin{minipage}{0.8\textwidth}
\begin{verbatim}
        Integer myArray[] = new Integer[nodeIds()];
        for_nodes(v) { myArray[id(v)] = 1; }
}
\end{verbatim}
\end{minipage}

is immune to array out of bounds errors.

\subsection{Node and edge methods}

Nodes and edges have `getters' and `setters' for
a variety of attributes, i.e.,
\\
\texttt{set}$a$\texttt{($\langle a's$ type$\rangle$ x)}
\\
and
\\
$\langle a's$ type$\rangle$ \texttt{get}$a$\texttt{()},
where $a$ is the name of an attribute such as
\texttt{Color},
\texttt{Label} or \texttt{Weight}.
A more convenient way to access these standard attributes omits the prefix \texttt{get}
and uses procedural syntax:
\texttt{color(}$x$\texttt{)} is a synonym for $x$\texttt{.getColor()}, for example.
Procedural syntax for the setters is also available:
\texttt{setColor(}$x$,$c$\texttt{)} is a synonym for $x$.\texttt{setColor(}$c$\texttt{)}.
In the special cases of color and label it is possible to omit the \texttt{set}
(since it reads naturally): \texttt{color(}$x$,$c$\texttt{)} instead of \texttt{setColor(}$x$,$c$\texttt{)}.

\subsubsection{Logical attributes: functions and macros}

\textbf{Nodes.}
From a node's point of view we would like information about the adjacent nodes and incident edges.
The relevant \emph{methods} require the use of Java generics, but macros are provided
to simplify graph API access. The macros, which have equivalents in GDR, are:

\begin{itemize}

\item
\texttt{for\_adjacent(x, e, y)\{ \emph{code block} \}}
executes the statements in the code block for each edge incident on \verb$x$.
The statements can refer to \verb$x$, or \verb$e$, the current incident edge,
or \verb$y$, the other endpoint of \verb$e$.
The macro assumes that \texttt{x} has already been declared as \texttt{Node}
but \texttt{e} and \texttt{y} are declared automatically.

\item
\texttt{for\_outgoing(Node x, Edge e, Node y)\{ \emph{code block} \}}\\
behaves like \texttt{for\_adjacent} except that, when the graph is directed,
it iterates only over the edges whose source is \verb$x$ (it still iterates over all the edges when the graph is undirected). 

\item
\texttt{for\_incoming(Node x, Edge e, Node y)\{ \emph{code block} \}}\\
behaves like \texttt{for\_adjacent} except that, when the graph is directed,
it iterates only over the edges whose sink is \verb$x$ (it still iterates over all the edges when the graph is undirected). 

\end{itemize}

The actual API methods hiding behind these macros are (these are Node methods):

\begin{itemize}
\item
\texttt{List<Edge>~getIncidentEdges()} returns a list of all edges incident to this node,
both incoming and outgoing.
\item
\texttt{List<Edge>~getOutgoingEdges()} returns a list of edges directed away
from this node (all incident edges if the graph is undirected).
\item
\texttt{List<Edge>~getIncomingEdges()} returns a list of edges directed toward
this node (all incident edges if the graph is undirected).
\item
\texttt{Node~travel(Edge~e)} returns the other endpoint of \texttt{e}.
\end{itemize}

The above all use Java syntax, as in \texttt{v.travel(e)}.
The following are node-related functions with procedural syntax.

\begin{itemize}
\item \texttt{degree(v)}, \texttt{indegree(v)} and \texttt{outdegree(v)} return the appropriate
integers.
\item \texttt{otherEnd(v, e)}, where \texttt{v} is a node and \texttt{e} is an edge
returns node \texttt{w} such that \texttt{e} connects \texttt{v} and \texttt{w};
the programmer can also say \texttt{otherEnd(e, v)} in case she forgets the order
of the arguments.
\item \texttt{neighbors(v)} returns a list of the nodes adjacent to node \texttt{v}.
\end{itemize}

\bigskip
\textbf{Edges.}
The logical attributes of an edge are its source and target (destination).

\begin{itemize}
\item
\texttt{setSourceNode(Node)} and \texttt{Node~getSourceNode()}
\item
\texttt{setTargetNode(Node)} and \texttt{Node~getTargetNode()}
\item
\texttt{getOtherEndPoint(Node~u)} returns \texttt{v} where this edge is
either \texttt{uv} or \texttt{vu}.
\end{itemize}

\bigskip
\textbf{Graph Elements.}
Nodes and edges both have a mechanism for setting (and getting)
arbitrary attributes of type Integer, String, and Double.
the relevant methods are\\
\texttt{setIntegerAttribute(String~key,Integer~value)}\\ 
to associate an integer value with a node and\\
\texttt{Integer~getIntegerAttribute(String~key)}\\
to retrieve it.
String and Double attributes work the same way as integer attributes.
These are useful when an algorithm requires arbitrary information to be
associated with nodes and/or edges.
The user-defined attributes may differ from one node or edge to the next.
For example, some nodes may have a \texttt{depth} attribute while others do not.

\subsubsection{Geometric attributes}

Currently, the only geometric attributes are the positions of the
nodes. 
Unlike GDR, the edges in Galant
are all straight lines and the positions of their labels are fixed.
The relevant methods for nodes -- using procedural syntax -- are
\texttt{int~getX(Node)}, \texttt{int~getY(Node)}
and \texttt{Point~getPosition(Node)}
for the 'getters'. To set a position,
one should use\\
\hspace*{2em}\texttt{setPosition(Node,Point)}\\
or\\
\hspace*{2em}\texttt{setPosition(Node,int,int)}.\\
Once a node has an established position, it is possible to change
only one coordinate using \texttt{setX(Node,int)} or \texttt{setY(Node,int)}.
Object-oriented variants of all of these, e.g.,
\texttt{v.setX(100)}, are also available.

Ordinarily the user can move nodes during algorithm execution
and the resulting positions persist after execution terminates.
For some algorithms, such as sorting, the algorithm itself needs to move
nodes.
It is desirable then to keep the user from moving nodes.
The declaration \texttt{movesNodes()} at the beginning of an algorithm
accomplishes this.

\subsubsection{Display attributes} \label{sec:display_attributes}

Each node and edge has
both a (double) weight and a label.
The weight
is also a logical
attribute in that
it is used implicitly as a
key for
sorting and priority queues.
The label is simply text and may be interpreted however the programmer
chooses.
The conversion functions \texttt{integer(String)}
and \texttt{real(String)}
-- see Table~\ref{tab:utility_functions}
-- provide a convenient mechanism for treating labels as objects of class
\texttt{Integer} or \texttt{Double}, respectively.
The argument of \texttt{setLabel} and its relatives -- see below
-- is not the expected \texttt{String}
but \texttt{Object};
any type of object that has a Java \texttt{toString} method will work
-- numbers have to be of type \texttt{Integer} or \texttt{Double}
rather than \texttt{int} or \texttt{double} since the latter are not objects
in Java.\footnote{
  Galant functions return objects, \texttt{Integer} or \texttt{Double}, when
  return values are numbers for this reason.
}
So conversion between string labels and numbers works both ways.

Aside from the setters and getters: \texttt{setWeight(double)},
\mbox{\texttt{Double getWeight()}}, \texttt{setLabel(Object)}
and \mbox{\texttt{String getLabel()}}, the programmer can also
manipulate and test for the absence of weights/labels using
\texttt{clearWeight()} and \texttt{boolean~hasWeight()},
and the corresponding methods for labels.
The procedural variants in this case are
\texttt{setWeight(Node,double)},
\mbox{\texttt{Double weight(Node)}},\footnote{
  The \emph{get} is omitted here for more natural syntax.}
\texttt{label(Node,Object)},\footnote{
  A natural syntax that resembles English. However,
  \texttt{setLabel(Node,Object)} is also allowed.
}
and \mbox{\texttt{String label(Node)}}

Nodes can either be plain, highlighted (selected), marked (visited) or both highlighted and
marked.
Being highlighted alters the
the boundary (color and thickness) of a node (as controlled by the
implementation), while being marked affects the fill color.
Edges can be plain or selected, with thickness and color modified in the
latter case.

The relevant methods are
(here \texttt{Element} refers to either a \texttt{Node} or an \texttt{Edge}):
\begin{itemize}
\item \texttt{highlight(Element)}, \texttt{unhighlight(Element)}
  and \texttt{Boolean~isHighlighted(Element)}
\item correspondingly, \texttt{setSelected(true)}, \texttt{setSelected(false)},
and \texttt{boolean~isSelected()}
\item \texttt{mark(Node)}, \texttt{unmark(Node)}
  and \texttt{Boolean~isMarked(Node)},
  equivalently \texttt{Boolean~marked(Node)}.
\end{itemize}

\begin{table}
  \centering
  \begin{tabular}{{| l @{~=~} l |}}
    \hline
    \texttt{RED} & \texttt{"\#ff0000"} \\ \hline
    \texttt{BLUE} & \texttt{"\#00ff00"} \\ \hline
    \texttt{GREEN} & \texttt{"\#0000ff"} \\ \hline
    \texttt{YELLOW} & \texttt{"\#ffff00"} \\ \hline
    \texttt{MAGENTA} & \texttt{"\#ff00ff" } \\ \hline
    \texttt{CYAN} & \texttt{"\#00ffff"} \\ \hline
    \texttt{TEAL} & \texttt{"\#009999"} \\ \hline
    \texttt{VIOLET} & \texttt{"\#9900cc"} \\ \hline
    \texttt{ORANGE} & \texttt{"\#ff8000"} \\ \hline
    \texttt{GRAY} & \texttt{"\#808080"} \\ \hline
    \texttt{BLACK} & \texttt{"\#000000"} \\ \hline
    \texttt{WHITE} & \texttt{"\#ffffff"} \\ \hline
  \end{tabular}
  \caption{Predefined color constants.}
  \label{tab:colors}
\end{table}

Although the specific colors for displaying the outlines of nodes
or the lines representing edges are
predetermined for plain
and highlighted nodes/edges,
the animation implementation can modify these colors,
thus allowing for many different kinds of highlighting.
The \texttt{getColor} and \texttt{setColor} methods
and their procedural variants have \texttt{String} arguments
in the RGB format \textsf{\#RRGGBB}; for example,
the string \texttt{\#0000ff} is blue.
The predefined color constants are listed in Table~\ref{tab:colors}.

Of the attributes listed above, weight, label, color and position can be
accessed and modified by the user as well as the program.
In all cases, modifications by execution of the animation are ephemeral
-- the graph returns to its original state after execution.
The user can save the mid-execution state of the graph:
select the \texttt{Export} option on the file menu of the
\emph{graph} window.

\input{Y_graph_element_functions}

A summary of functions relevant to node and edge attributes (their procedural versions)
is given in Table~\ref{tab:graph_element_functions}.

\subsubsection{Global access for individual node/edge attributes and graph attributes}

\input{Y_graph_attribute_functions}

It is sometimes useful to access or manipulate attributes of nodes and edges
globally.
For example, an algorithm might want to hide node weights entirely
because they are not relevant
or hide them initially and reveal them for individual nodes as
the algorithm progresses.
These functionalities can be accomplished by
\textsf{hideNodeWeights} or \textsf{hideAllNodeWeights}, respectively.
A summary of these capabilities is given in Table~\ref{tab:graph_attribute_functions}.

\subsection{Additional programmer information}

A Galant algorithm/program is executed as a method within a Java class.
In order to shield the Galant programmer from Java idiosyncrasies,
some features have been added.

\subsubsection{Definition of Functions/Methods}\label{sec:functions}

A programmer can define arbitrary functions (methods) using the construct

\texttt{function} \textsl{[return\_type]} \textsl{name} \texttt{(}
 \textsl{parameters} \texttt{) \{} \\
 \hspace*{3em} \emph{code block} \\
 \texttt{\}}

The behavior is like that of a Java method. So, for example,
\begin{verbatim}
function int plus( int a, int b ) {
    return a + b;
}
\end{verbatim}
is equivalent to
\begin{verbatim}
static int plus( int a, int b ) {
    return a + b;
}
\end{verbatim}

The \textsl{return\_type} is optional. If it is missing, the function behaves like
a \textsf{void} method in Java. An example is the recursive
\\
\texttt{function visit( Node v ) \{} \textsl{code} \texttt{\}}
\\
The conversion of functions into Java methods when Galant code is compiled
is complex and may result in indecipherable error messages.

\input{Y_data_structures}

\subsubsection{Data Structures}

Galant provides some standard data structures for nodes and edges automatically.
These are described in detail in Table~\ref{tab:data_structures}.
Data structures use object-oriented syntax.
For example, to add a node \texttt{v} to a \texttt{NodeList} \texttt{L},
the appropriate syntax is \texttt{L.add(v)}.
Most data structure operations are as efficient as one might expect.
A notable exception is \texttt{decreaseKey}
for priority queues.
The operation \texttt{pq.decreaseKey(v,k)}
does \texttt{pq.remove(v)} followed by \texttt{pq.add(v)}.
The new key is implicit -- it is the weight of the element.
In practice, assuming \texttt{pq} is a \texttt{NodePriorityQueue},
\texttt{v} is a \texttt{Node}
and \texttt{k} is a \texttt{Double}, the sequence would be
\begin{verbatim}
     setWeight(v, k);
     pq.decreaseKey(v,k);
\end{verbatim}
which would translate to
\begin{verbatim}
     setWeight(v, k);
     pq.remove(v);
     pq.add(v)
\end{verbatim}
As pointed out above, the weight of a node or edge is used for priority in a priority
queue or for sorting.
The programmer can change this default
by redefining the Java \texttt{compareTo()} method,
but this requires finesse and Java expertise.
