/**
 * Used to jump to the end of the run method -- not a real exception
 */
package edu.ncsu.csc.Galant.algorithm;

public class Terminate extends Exception {
}

//  [Last modified: 2015 12 02 at 13:20:43 GMT]
