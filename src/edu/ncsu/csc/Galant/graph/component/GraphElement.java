package edu.ncsu.csc.Galant.graph.component;

import java.util.List;
import java.util.ArrayList;

import edu.ncsu.csc.Galant.GalantException;
import edu.ncsu.csc.Galant.GraphDispatch;
import edu.ncsu.csc.Galant.logging.LogHelper;
import edu.ncsu.csc.Galant.algorithm.Terminate;

/**
 * Abstract class containing graph element manipulation methods
 * @author Jason Cockrell, Ty Devries, Alex McCabe, Michael Owoc,
 *         completely rewritten by Matthias Stallmann
 */
public class GraphElement implements Comparable<GraphElement> {

    public static final String ID = "id";
    public static final String WEIGHT = "weight";
    public static final String LABEL = "label";
    public static final String COLOR = "color";
    public static final String DELETED = "deleted";
    public static final String HIGHLIGHTED = "highlighted";
    public static final String HIDDEN = "hidden";
    public static final String HIDDEN_LABEL = "hiddenLabel";
    public static final String HIDDEN_WEIGHT = "hiddenWeight";
    
    /**
     * The graph with which this element is associated.
     */
    protected Graph graph;

    protected GraphDispatch dispatch;

    /**
     * The list of states that this element has been in up to this point --
     * essentially the list of all changes.
     */
	protected List<GraphElementState> states;

    /**
     * Constructor to be used during parsing; all additional information is
     * filled in by initializeAfterParsing(). The algorithm state is
     * initialized elsewhere, currently in Graph.
     */
    public GraphElement(Graph graph) {
        this.dispatch = GraphDispatch.getInstance();
        this.states = new ArrayList<GraphElementState>();
        this.graph = graph;
        this.addState(new GraphElementState());
    }

    /**
     * Resets this element to its original state at the end of an animation.
     * @param graphState the initial state of the graph containing this element
     */
    protected void reset() {
        ArrayList<GraphElementState> initialStates
            = new ArrayList<GraphElementState>();
        for ( GraphElementState state : this.states ) {
            if ( state.getState() > 0 ) break;
            initialStates.add(state);
        }
        this.states = initialStates;
    }

    /**
     * @return a new state for this element; the new state will be identical
     * to the current (latest one) except that it will be tagged with the
     * current algorithm state if the algorithm is running; subsequent
     * changes to this GraphElement will take place in the new state.
     *
     * @todo there is no reason to create new states when parsing and the
     * only reason to do it when editing is for a possible "undo" mechanism,
     * which is not yet implemented
     */
    private GraphElementState newState() throws Terminate {
		dispatch.startStepIfRunning();
		GraphElementState latest = latestState();
		GraphElementState elementState
            = new GraphElementState(latest);
		
        LogHelper.logDebug( "newState (element) = " + elementState.getState() );
		return elementState;
    }

    /**
     * @return The last state on the list of states. This is the default for
     * retrieving information about any attribute.
     */
    public GraphElementState latestState() {
        LogHelper.enterMethod(getClass(), "latestState, states = " + states);
        GraphElementState state = null;
        if ( states.size() != 0 ) {
            state = states.get(states.size() - 1);
        }
        LogHelper.exitMethod(getClass(), "latestState, state = " + state);
        return state; 
    }

    /**
     * This method is vital for retrieving the most recent information about
     * a graph element (node or edge), where most recent is defined relative
     * to a given time stamp, as defined by forward and backward stepping
     * through the animation.
     * @see edu.ncsu.csc.Galant.algorithm.AlgorithmExecutor
     * @param stateNumber the numerical indicator (timestamp) of a state,
     * usually the current display state
     * @return the latest instance of GraphElementState that was created before the
     * given time stamp, or null if the element did not exist before the time
     * stamp.
     */
	public GraphElementState getLatestValidState(int stateNumber) {
        LogHelper.enterMethod(getClass(), "getLatestValidState("
                              + stateNumber + "), " + this);
        GraphElementState toReturn = null;
        int stateIndex = states.size() - 1;
		while ( stateIndex >= 0 ) {
			GraphElementState state = states.get(stateIndex);
			if ( state.getState() <= stateNumber ) {
				toReturn = state;
                break;
			}
            stateIndex--;
		}
        LogHelper.exitMethod(getClass(), "getLatestValidState("
                              + stateNumber + "), " + toReturn);
        return toReturn;
	}
	
	/**
     * Adds the given state to the list of states for this element. If there
     * is already a state having the same algorithm state (time stamp), there
     * is no need to add another one. Such a situation might arise if there
     * are multiple state changes to this element between a
     * beginStep()/endStep() pair or if no algorithm is running.  If an
     * algorithm is running, this method initiates synchronization with the
     * master thread to indicate that the changes corresponding to the added
     * state are completed
     *
     * @invariant states are always sorted by state number.
     */
	private void addState(GraphElementState stateToAdd) {
        LogHelper.enterMethod(getClass(), "addState, state number = "
                              + stateToAdd.getState());
        int stateNumber = stateToAdd.getState();
        boolean found = false;
        for ( int i = states.size() - 1; i >= stateNumber; i-- ) {
            GraphElementState state = states.get(i);
            LogHelper.logDebug("addState loop, i = " + i + ", state(i) = " + state.getState());
            if ( state.getState() == stateNumber ) {
                states.set(i, stateToAdd);
                found = true;
                break;
            }
        }
        if ( ! found ) {
            states.add(stateToAdd);
            dispatch.pauseExecutionIfRunning();
        }
        LogHelper.exitMethod(getClass(), "addState, found = " + found);
	}

    /************** Integer attributes ***************/
	public boolean set(String key, Integer value) throws Terminate {
        GraphElementState newState = newState();
        boolean found = newState.set(key, value);
        addState(newState);
        return found;
	}
	public Integer getInteger(String key) {
        GraphElementState state = latestState();
        if ( state == null ) return null;
		return state.getAttributes().getInteger(key);
	}
	public Integer getInteger(int state, String key) {
        GraphElementState validState = getLatestValidState(state);
		return validState == null ? null : validState.getAttributes().getInteger(key);
	}


    /************** Double attributes ***************/
	public boolean set(String key, Double value) throws Terminate {
        GraphElementState newState = newState();
        boolean found = newState.set(key, value);
        addState(newState);
        return found;
	}
	public Double getDouble(String key) {
        GraphElementState state = latestState();
        if ( state == null ) return null;
		return state.getAttributes().getDouble(key);
	}
	public Double getDouble(int state, String key) {
        GraphElementState validState = getLatestValidState(state);
		return validState == null ? null : validState.getAttributes().getDouble(key);
	}

    /************** Boolean attributes ***************/
	public boolean set(String key, Boolean value) throws Terminate {
        LogHelper.enterMethod(getClass(),
                              "set, key = " + key + ", value = " + value);
        GraphElementState newState = newState();
        boolean found = newState.set(key, value);
        addState(newState);
        LogHelper.exitMethod(getClass(),
                              "set, object = " + this);
        return found;
	}

    /**
     * If value is not specified, assume it's boolean and set to true
     */
    public boolean set(String key) throws Terminate {
        return this.set(key, true);
    }
 
    public void clear(String key) throws Terminate {
        this.remove(key);
    }

    /**
     * For boolean attributes, assume that the absense of an attribute means
     * that it's false.
     */
	public Boolean getBoolean(String key) {
        GraphElementState state = latestState();
        if ( state == null ) return false;
		return state.getAttributes().getBoolean(key);
	}
	public Boolean getBoolean(int state, String key) {
        GraphElementState validState = getLatestValidState(state);
		return validState == null ? false : validState.getAttributes().getBoolean(key);
	}

    /**
     * Synonyms (for readability in algorithms)
     */
    public Boolean is(String key) {
        return getBoolean(key);
    }
    public Boolean is(int state, String key) {
        return getBoolean(state, key);
    }
    
    /************** String attributes ***************/
	public boolean set(String key, String value) throws Terminate {
        GraphElementState newState = newState();
        boolean found = newState.set(key, value);
        addState(newState);
        return found;
	}
	public String getString(String key) {
        GraphElementState state = latestState();
        if ( state == null ) return null;
		return state.getAttributes().getString(key);
	}
	public String getString(int state, String key) {
        GraphElementState validState = getLatestValidState(state);
		return validState == null ? null : validState.getAttributes().getString(key);
	}

    /**
     * Removes the attribute with the given key from the list and updates
     * state information appropriately.
     */
    public void remove(String key) throws Terminate {
        GraphElementState newState = newState();
        newState.remove(key);
        addState(newState);
    }
	
    public boolean isDeleted() {
        return getBoolean(DELETED);
    }
    public boolean isDeleted(int state) {
        return getBoolean(state, DELETED);
    }
    /**
     * @param true iff this element is to be deleted in the current state.
     */
    public void setDeleted(boolean deleted) throws Terminate {
        if (deleted) {
            set(DELETED, true);
        }
        else remove(DELETED);
    }

    /**
     * @return true if this element existed in the latest state prior to the
     * given one.
     */
    public boolean isCreated(int state) {
        GraphElementState creationState = getLatestValidState(state);
        return creationState == null ? false : true;
    }

    /**
     * @return true if the element has been created but has not been
     * deleted.
     */
	public boolean inScope() {
		return ! isDeleted();
	}
	
	public boolean inScope(int state) {
		return isCreated(state) && ! isDeleted(state);
	}
	
    /**
     * @return true if this element is hidden, i.e., will not be drawn on the
     * graph panel.
     */
    public Boolean isHidden(int state) {
        return getBoolean(state, HIDDEN);
    }
    public void hide() throws Terminate { set(HIDDEN); }
    public void show() throws Terminate { clear(HIDDEN); }


    /**************************** weights **************************/
	public Double getWeight() {
        return getDouble(WEIGHT);
    }
	public Double getWeight(int state) {
        return getDouble(state, WEIGHT);
    }

    /**
     * Need to convert the argument to a double because it eventually
     * converts to Double -- see cleanupAfterParsing()
     */
	public void setWeight(double weight) throws Terminate {
        set(WEIGHT, (double) weight);
    }

    public boolean hasWeight() {
        return getWeight() != null;
    }
    public boolean hasWeight(int state) {
        return getWeight(state) != null;
    }
    /**
     * removes the weight from the property list
     */
    public void clearWeight() throws Terminate {
        remove(WEIGHT);
    }
	
    /**
     * @return true if the weight of this element is hidden, i.e., will not be
     * drawn on the graph panel.
     */
    public Boolean weightIsHidden(int state) {
        return getBoolean(state, HIDDEN_WEIGHT);
    }
    public void hideWeight() throws Terminate { set(HIDDEN_WEIGHT); }
    public void showWeight() throws Terminate { clear(HIDDEN_WEIGHT); }


    /**************************** labels *************************/
	public String getLabel() {
        return getString(LABEL);
    }
	public String getLabel(int state) {
        return getString(state, LABEL);
    }

	public void setLabel(String label) throws Terminate {
        set(LABEL, label);
    }

    public boolean hasLabel() {
        return getLabel() != null;
    }
    public boolean hasLabel(int state) {
        return getLabel(state) != null;
    }
    /**
     * removes the label from the property list
     */
    public void clearLabel() throws Terminate {
        remove(LABEL);
    }
	
    /**
     * @return true if the label of this element is hidden, i.e., will not be
     * drawn on the graph panel.
     */
    public Boolean labelIsHidden(int state) {
        return getBoolean(state, HIDDEN_LABEL);
    }
    public void hideLabel() throws Terminate { set(HIDDEN_LABEL); }
    public void showLabel() throws Terminate { clear(HIDDEN_LABEL); }

    /**************************** colors *************************/
	public String getColor() {
        return getString(COLOR);
    }
	public String getColor(int state) {
        return getString(state, COLOR);
    }

	public void setColor(String color) throws Terminate {
        set(COLOR, color);
    }

    public boolean hasColor() {
        return getColor() != null;
    }
    public boolean hasColor(int state) {
        return getColor(state) != null;
    }
    /**
     * removes the color from the property list
     */
    public void clearColor() throws Terminate {
        remove(COLOR);
    }

    /**************************** highlighting ***********************/
    /**
     * In case it matters, setSelected simply changes the value of the
     * HIGHLIGHTED attribute (if it's already there) while unHighlight
     * actually removes the entry corresponding to that attribute. The effect
     * is the same, but the nature of the list traversal might not be.
     */
	public boolean isSelected() {
        return getBoolean(HIGHLIGHTED);
    }
	public Boolean isSelected(int state) {
        return getBoolean(state, HIGHLIGHTED);
    }
	public void setSelected(Boolean highlighted) throws Terminate {
        set(HIGHLIGHTED, highlighted);
    }
	public boolean isHighlighted() {
        return getBoolean(HIGHLIGHTED);
    }
	public Boolean isHighlighted(int state) {
        return getBoolean(state, HIGHLIGHTED);
    }
	public void highlight() throws Terminate {
        set(HIGHLIGHTED, true);
    }
	public void unHighlight() throws Terminate {
        remove(HIGHLIGHTED);
    }
    // alternate spelling
	public void unhighlight() throws Terminate {
        remove(HIGHLIGHTED);
    }

    /**
     * Parses specific attributes that are not to be stored internally as
     * strings.  This allows the GraphMLParser to create each element without
     * knowing its attributes, then collect them in order of appearance, and
     * finally postprocess so that the essential ones are initialized
     * properly. Also establishes the initial state for this element.
     *
     * Relevant attributes for all graph elements are ...
     * - id: integer
     * - weight: double
     * - highlighted: boolean
     * - deleted: boolean (not clear if this ever becomes an issue; not implemented)
     * 
     * Attributes for nodes are ...
     * - x, y: integer
     * - layer, positionInLayer: integer
     * - marked: boolean
     *
     * Attributes for edges are ...
     * - source, target: integer (id's of nodes)
     */
    public void initializeAfterParsing() throws GalantException {
        try { // need to catch Terminate exception -- should not happen
            String idString = getString(ID);
            if ( idString != null ) {
                Integer id = Integer.MIN_VALUE;
                try {
                    id = Integer.parseInt(idString);
                }
                catch ( NumberFormatException e ) {
                    throw new GalantException("Bad id " + idString);
                }
                remove(ID);
                set(ID, id); 
            }
            String weightString = getString(WEIGHT);
            if ( weightString != null ) {
                Double weight = Double.NaN;
                try {
                    weight = Double.parseDouble(weightString);
                }
                catch ( NumberFormatException e ) {
                    throw new GalantException("Bad weight " + weightString);
                }
                remove(WEIGHT);
                set(WEIGHT, weight); 
            }
            String highlightString = getString(HIGHLIGHTED);
            if ( highlightString != null ) {
                Boolean highlighted = Boolean.parseBoolean(highlightString);
                remove(HIGHLIGHTED);
                if ( highlighted )
                    set(HIGHLIGHTED, highlighted); 
            }
        }
        catch ( Terminate t ) {
            // should not happen
            t.printStackTrace();
        }
    }

    /**
     * Creates a string that can be used to form the "interior" of a GraphML
     * representation of the attributes associated with this state.
     */
    public String xmlString() {
        GraphElementState elementState = latestState();
        if ( elementState == null ) return "";
        return elementState.xmlString();
    }

    /**
     * Like xmlString(), except that it omits the "x" and "y" attributes; to
     * be used in cases where these attributes are superceded by the
     * corresponding fixed ones of a Node.
     */
    public String attributesWithoutPosition() {
        GraphElementState elementState = latestState();
        if ( elementState == null ) return "";
        return elementState.attributesWithoutPosition();
    }

    /**
     * Like xmlString(), except that it omits the "id" attribute; to be used
     * in cases where the id is optional, as is the case with an Edge
     */
    public String attributesWithoutId() {
        GraphElementState elementState = latestState();
        if ( elementState == null ) return "";
        return elementState.attributesWithoutId();
    }

    /**
     * Same as the unparameterized version except that the attributes are
     * ones of the latest valid state. This is used when exporting the state
     * of the graph in the middle of execution.
     */
    public String xmlString(int state) {
        GraphElementState elementState = getLatestValidState(state);
        if ( elementState == null ) return "";
        return elementState.xmlString();
    }

    /**
     * Same as the unparameterized version except that the attributes are
     * ones of the latest valid state. This is used when exporting the state
     * of the graph in the middle of execution.
     */
    public String attributesWithoutPosition(int state) {
        GraphElementState elementState = getLatestValidState(state);
        if ( elementState == null ) return "";
        return elementState.attributesWithoutPosition();
    }

    /**
     * Same as the unparameterized version except that the attributes are
     * ones of the latest valid state. This is used when exporting the state
     * of the graph in the middle of execution.
     */
    public String attributesWithoutId(int state) {
        GraphElementState elementState = getLatestValidState(state);
        if ( elementState == null ) return "";
        return elementState.attributesWithoutId();
    }

	public int compareTo(GraphElement other) {
        Double thisDouble = new Double( this.getWeight() );
        Double otherDouble = new Double( other.getWeight() );
		return thisDouble.compareTo( otherDouble );
	}
}

//  [Last modified: 2016 06 21 at 16:49:17 GMT]
