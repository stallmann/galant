*** more recent releases are available at https://github.com/mfms-ncsu/galant ***

galant
======

This software is licensed by a Gnu Public License. See
https://www.gnu.org/licenses/gpl.html
for details.
Copyright Matthias F. Stalmann, 2016, all rights reserved.
Contact author home page: people.engr.ncsu.edu/mfms

To compile everything and run the program for the first time, simply type
ant
while in the home directory.
After that you can run Galant, either by saying
ant run
or by executing
build/jar/Galant.jar

*** Testing *** (see testingGalant.docx for more details)

First set the home directory for opening and saving files
      File -> Preferences -> Open/Save
to the top level directory of Galant so that all the relevant files
are easy to get to.

Then do the following test runs. Each time, open the algorithm and the graph
files and first hit compile when the text window shows the algorithm and Run
when the text window shows the algorithm and the graph window shows the
graph.

Algorithm                                     Graph

Algorithms/dfs_d.alg                            eight_node_graph.graphml

Algorithms/dijkstra.alg                         weighted_example.graphml
     - try both undirected and directed

Algorithms/insertion_sort.alg                   sorting_test.graphml
     - show vertex weights

Algorithms/binary_tree.alg                      empty graph

For more information and links to important resources, see 0-index.html.




