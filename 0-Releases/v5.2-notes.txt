New in release 5.2:

- smart repositioning (force directed) now handles multiple components correctly
- it is now possible to undo smart repositioning of nodes by simply toggling the smart reposition button
